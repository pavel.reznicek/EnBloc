#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Various constants used throughout En bloc."""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import os
import platform
import sys
import wx

_ = wx.GetTranslation

#STONE_DIM = 15
STONE_DIM = 32

#SK_DIAGONAL = 0
#SK_VARIANT = 1
#SK_FLAT = 2
#SK_BOMB = 3
SK_SPIRAL = 0

STONE_KINDS = [
    # SK_DIAGONAL,
    # SK_VARIANT,
    # SK_FLAT,
    # SK_BOMB
    SK_SPIRAL
]
MIN_STONE_KIND = 0
MAX_STONE_KIND = len(STONE_KINDS) - 1

SC_RED = 0
SC_GREEN = 1
SC_BLUE = 2
SC_GRAY = 3
SC_YELLOW = 4
SC_WHITE = 5

STONE_COLOURS = [
    SC_RED,
    SC_GREEN,
    SC_BLUE,
    SC_GRAY,
    SC_YELLOW,
    SC_WHITE
]

EK_DIAGONAL = 0
EK_VARIANT = 1
EK_FLAT = 2

EXPLOSION_KINDS = [
    EK_DIAGONAL,
    EK_VARIANT,
    EK_FLAT
]

EXPLOSION_INTERVAL = 1000 // 3  # 1/3 s

def new_id():
    """Returns a new wxPython ID"""
    if hasattr(wx, "NewIdRef"):
        return wx.NewIdRef()
    else:
        return wx.NewId()

GM_DETONATOR = new_id()
GM_CASTLE = new_id()

# File & directory paths
APP_PATH = getattr(
    sys, '_MEIPASS',  # pylint: disable=protected-access
    os.path.abspath(os.path.dirname(__file__))
)
SYSTEM = platform.system()
MACHINE = platform.machine()
PLATFORM = "-".join([SYSTEM, MACHINE])
LIBDIR = os.path.join("lib", PLATFORM)
LIBRARY_PATH = os.path.join(APP_PATH, LIBDIR)
BMP_PATH = os.path.join(APP_PATH, "images")
BMP_PATH_STONES = os.path.join(BMP_PATH, "stones.png")
BMP_PATH_TRANSPARENT_STONES = os.path.join(BMP_PATH, "transparent stones.png")
BMP_PATH_EXPLOSIONS = os.path.join(BMP_PATH, "explosions.png")
BMP_PATH_BACKGROUND = os.path.join(BMP_PATH, "background.png")
BMP_PATH_NEW_GAME = os.path.join(BMP_PATH, "document_16.png")
BMP_PATH_SETTINGS = os.path.join(BMP_PATH, "gear_16.png")
BMP_PATH_HELP = os.path.join(BMP_PATH, "help_16.png")
BMP_PATH_CLOSE = os.path.join(BMP_PATH, "close_16.png")
BMP_PATH_DETONATOR = os.path.join(BMP_PATH, "detonator.png")
BMP_PATH_CASTLE = os.path.join(BMP_PATH, "castle.png")
HTML_PATH_README = os.path.join(APP_PATH, _("README.html"))
HTML_PATH_LICENSE = os.path.join(APP_PATH, "LICENSE.html")
SND_DIR = "sounds"
SND_FILE_EXPLOSION = "explosion.wav"
SND_PATH = os.path.join(APP_PATH, SND_DIR)
SND_PATH_EXPLOSION = os.path.join(SND_PATH, SND_FILE_EXPLOSION)

get_international_html_path = lambda x: os.path.join(APP_PATH, wx.GetTranslation(x))
get_international_html_path_readme = lambda: get_international_html_path("README.html")

# language domain
LANGUAGE_DOMAIN = "en_bloc"
# languages you want to support
SUPPORTED_LANGUAGES = {
    "en": wx.LANGUAGE_ENGLISH,
    "cs": wx.LANGUAGE_CZECH,
}
