#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This will generate the .pot and .mo files for the application domain and
languages defined below.

The .po and .mo files are placed as per convention in

"appfolder/locale/lang/LC_MESSAGES"

The .pot file is placed in the locale folder.

This script or something similar should be added to your build process.

The actual translation work is normally done using a tool like poEdit or
similar, it allows you to generate a particular language catalog from the .pot
file or to use the .pot to merge new translations into an existing language
catalog.
"""

import os
import sys
import platform
import wx

from en_bloc import constants

import i18nbase

_ = wx.GetTranslation

class GenI18nApp(i18nbase.I18nBaseApp):  # pylint: disable=too-many-ancestors
    """The application to run the actual conversion"""
    def OnInit(self):
        """Called on the application start"""
        super().OnInit()
        # we remove English as source code strings are in English
        self.supported_languages = supported_languages_without_en()  # pylint: disable=attribute-defined-outside-init

        app_folder = self.app_folder = os.getcwd()  # pylint: disable=attribute-defined-outside-init

        # setup some stuff to get at Python I18N tools/utilities

        pyexe = self.pyexe = sys.executable  # pylint: disable=attribute-defined-outside-init
        on_windows = self.on_windows = platform.system() == 'Windows'  # pylint: disable=attribute-defined-outside-init
        if on_windows:
            pyfolder = os.path.split(pyexe)[0]
            pytoolsfolder = os.path.join(pyfolder, 'Tools')
            pyi18nfolder = os.path.join(pytoolsfolder, 'i18n')
            self.pygettext = os.path.join(pyi18nfolder, 'pygettext.py')  # pylint: disable=attribute-defined-outside-init
            self.pymsgfmt = os.path.join(pyi18nfolder, 'msgfmt.py')  # pylint: disable=attribute-defined-outside-init
        else:
            self.pygettext = 'pygettext3'  # pylint: disable=attribute-defined-outside-init
            self.pymsgfmt = 'msgfmt'  # pylint: disable=attribute-defined-outside-init
        self.out_folder = os.path.join(app_folder, 'locale')  # pylint: disable=attribute-defined-outside-init

        self.run_pygettext()
        self.replace_pot_encoding()
        self.run_msgfmt()

        return True

    def run_pygettext(self):
        """Build command for pygettext"""
        gtoptions = '-a -d %s -o %s.pot -p %s %s'
        if self.on_windows:
            cmd = self.pyexe + ' '
        else:
            cmd = ''
        cmd += self.pygettext + ' ' + (gtoptions % (
            constants.LANGUAGE_DOMAIN,
            constants.LANGUAGE_DOMAIN,
            self.out_folder,
            self.app_folder
        ))
        print(_("Generating the .pot file"))
        print(_("cmd: %s") % cmd)
        rcode = os.system(cmd)
        print(_("return code: %s\n\n") % rcode)

    def replace_pot_encoding(self):
        """Edits the resulting .pot file, replacing the dummy encoding with UTF-8"""
        print(_("Replacing the dummy encoding with UTF-8…\n\n"))
        pot_file_path = os.path.join(self.out_folder, constants.LANGUAGE_DOMAIN + '.pot')
        pot_file = open(pot_file_path, "r")
        text = pot_file.read()
        text = text.replace(
            r'"Content-Type: text/plain; charset=CHARSET\n"',
            r'"Content-Type: text/plain; charset=UTF-8\n"'
        )
        text = text.replace(
            r'"Content-Transfer-Encoding: ENCODING\n"',
            r'"Content-Transfer-Encoding: 8bit\n"'
        )
        pot_file.close()
        pot_file = open(pot_file_path, "w")
        pot_file.write(text)
        pot_file.close()


    def run_msgfmt(self):
        """Build command for msgfmt"""
        for lang in self.supported_languages:
            lang_dir = os.path.join(self.app_folder, 'locale', lang, 'LC_MESSAGES')
            po_file = os.path.join(lang_dir, constants.LANGUAGE_DOMAIN + '.po')
            mo_file = os.path.join(lang_dir, constants.LANGUAGE_DOMAIN + '.mo')
            if self.on_windows:
                cmd = self.pyexe + ' ' + self.pymsgfmt + ' ' + po_file + ' -o ' + mo_file
            else:
                cmd = self.pymsgfmt + ' ' + po_file + ' -o ' + mo_file
            print(_("Generating the .mo file"))
            print(_("cmd: %s") % cmd)
            rcode = os.system(cmd)
            print(_("return code: %s\n\n") % rcode)


def supported_languages_without_en():
    """
    Gets the supported languages
    and filters out English
    """
    supported_languages = []
    for language in constants.SUPPORTED_LANGUAGES:
        if language != "en":
            supported_languages.append(language)
    return supported_languages

if __name__ == '__main__':
    app = GenI18nApp(redirect=False)
    app.MainLoop()
