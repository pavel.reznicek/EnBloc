#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A dialog used to select the new game mode"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import wx
from en_bloc import constants
from en_bloc import i18nbase

_ = wx.GetTranslation


class GameModeDlg(wx.Dialog):  # pylint: disable=too-many-ancestors,too-few-public-methods
    """A dialog used to select the new game mode"""

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent=parent, title=_("New Game"), style=wx.DEFAULT_DIALOG_STYLE)

        szr = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(szr)

        bmp_detonator = wx.Bitmap(constants.BMP_PATH_DETONATOR)
        btn_detonator = self.btn_detonator = wx.Button(self, label=_("""&Detonator Mode
\t— Make explosions"""),
                                                       id=constants.GM_DETONATOR, style=wx.BU_LEFT)
        btn_detonator.Bitmap = bmp_detonator
        szr.Add(btn_detonator, 0, wx.GROW | wx.TOP | wx.LEFT | wx.RIGHT, 16)
        btn_detonator.Bind(wx.EVT_BUTTON, self.on_button)

        bmp_castle = wx.Bitmap(constants.BMP_PATH_CASTLE)
        btn_castle = self.btn_castle = wx.Button(self, label=_("""&Castle Mode
\t— Build castles, avoiding explosions"""),
                                                 id=constants.GM_CASTLE, style=wx.BU_LEFT)
        btn_castle.SetBitmap(bmp_castle)
        szr.Add(btn_castle, 0, wx.GROW | wx.TOP | wx.LEFT | wx.RIGHT, 16)
        btn_castle.Bind(wx.EVT_BUTTON, self.on_button)

        szr.AddSpacer(16)

        szr.Fit(self)

        self.Bind(wx.EVT_SHOW, self.on_show)

    def on_button(self, event):
        """The button click event handler"""
        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()

    def on_show(self, event):
        """The show event handler"""
        if self.Parent:  # pylint: disable=using-constant-test
            pos = self.Parent.Position  # pylint: disable=no-member
        else:
            pos = wx.Point(0, 0)
        if (pos.x == 0) and (pos.y == 0):
            self.CentreOnScreen()
        else:
            self.CentreOnParent()
        event.Skip()



if __name__ == '__main__':
    class TestApp(i18nbase.I18nBaseApp):  # pylint: disable=too-many-ancestors
        """An application to test out the game mode dialog"""

        def OnInit(self):  # pylint: disable=invalid-name,no-self-use
            """The application initialisation handler"""
            super().OnInit()
            dlg = GameModeDlg(None)
            modal_result = dlg.ShowModal()  # modal result
            print(_("Modal Result:"), modal_result)
            return True


    APP = TestApp()
