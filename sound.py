#!/usr/bin/env python3

"""A simple sound player.
Thanks to Lars Magnus Nordeide for inspiration
(see https://larsnordeide.com/python/pysdl2-playing-a-sound-from-a-wav-file-with-sdlmixer.html)."""

import os
import wx
from en_bloc import constants
os.environ["PYSDL2_DLL_PATH"] = constants.LIBRARY_PATH
try:
    import sdl2
    import sdl2.ext
    import sdl2.ext.compat
    import sdl2.sdlmixer
except ImportError:
    import traceback
    traceback.print_exc()

_ = wx.GetTranslation

class SoundPlayer():  # pylint: disable=too-many-ancestors
    """A simple class that loads a sound file upon creation
and plays it on demand."""
    def __init__(self, sound_file_name):
        self._sound_ok = False

        # PyInstaller path debugging
        sound_dir_path = constants.SND_PATH
        sound_file_path = constants.SND_PATH_EXPLOSION
        if os.path.exists(sound_dir_path):
            if not os.path.exists(sound_file_path):
                print(_("The sound file “%s” does not exist.")%sound_file_path)
                return
        else:
            print(_("The sound directory “%s” does not exist.")%sound_dir_path)
            return

        try:
            resources = sdl2.ext.Resources(constants.SND_PATH)
        except NameError:
            print(_("Could not import the PySDL2 library. Sound will be disabled."))
            return

        if sdl2.SDL_Init(sdl2.SDL_INIT_AUDIO) != 0:
            print(_("Cannot initialize audio system: {}").format(sdl2.SDL_GetError()))
            return

        if sdl2.sdlmixer.Mix_OpenAudio(44100, sdl2.sdlmixer.MIX_DEFAULT_FORMAT, 2, 1024):
            print(_("Cannot open mixed audio: {}").format(sdl2.sdlmixer.Mix_GetError()))
            return

        sound_file = resources.get_path(sound_file_name)
        sound = self._sound = sdl2.sdlmixer.Mix_LoadWAV(
            sdl2.ext.compat.byteify(sound_file, "utf-8")
        )
        if sound is None:
            print(_("Cannot open audio file: {}").format(sdl2.sdlmixer.Mix_GetError()))
            return
        self._sound_ok = True

    def __del__(self):
        sdl2.sdlmixer.Mix_CloseAudio()
        sdl2.SDL_Quit(sdl2.SDL_INIT_AUDIO)

    def play(self):
        """Plays the sound loaded upon creation"""
        if self._sound_ok:
            channel = sdl2.sdlmixer.Mix_PlayChannel(-1, self._sound, 0)
            if channel == -1:
                print(_("Cannot play sample: {}").format(sdl2.sdlmixer.Mix_GetError()))
                return
