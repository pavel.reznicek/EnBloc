#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""En bloc.
A game inspired by an old freeware game for Windows called Block Game
(which in turn is inspired by the Super Mario puzzle)."""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"
__version__ = "0.0"

__all__ = [
    'app',
    'blockstuff',
    'constants',
    'drawing',
    'playgroundstuff',
    'statuspanel',
    'stonestuff'
]
