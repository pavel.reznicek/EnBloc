# En bloc
A Falling Blocks Game

_En bloc_ aims to be a simple 2D game featuring falling blocks each built up of four square stones of two different colors. The player has to drive a falling block using the arrow (or A, W, S, and D) keys. If the block falls to the bottom of the game area, it's dropped there and a new block starts to fall and the player has to drive the new block.

As the blocks drop on the bottom, they build up a wall. In that wall, if four or more square stones of the same colour connect in a vertical or horizontal row on new block dropping, they explode and vanish. The stones that lie upon them fall down to take up their place.

## Authors and Contributors
Code by Pavel Řezníček A. K. A. Cigydd [kɪgǝð] – <pavel.reznicek@evangnet.cz>. See my other code [here](https://bitbucket.org/cigydd/).

Graphics by Marta Vostrá – see [www.mravkolev.cz](https://www.mravkolev.cz/) for examples of her work (in Czech).

Testing by Daniela Běťáková.

## Inspiration
This game is strongly inspired by an older Win32 free game called _Watson’s Block Game_ by _Johannes Wallroth,_ see
[www.programming.de](https://www.programming.de/index.php/archive#games-fun-stuff).

## Tools Used
_En bloc_ is written in [Python 3](https://www.python.org/) and its [wxPython](https://wxpython.org/) GUI framework.

Sound is done via the [PySDL2](https://pypi.org/project/PySDL2/) wrapper around the [SDL2 library](https://www.libsdl.org/).
The SDL2 and SDL2_mixer libraries get shipped inside the executable file.

The executable file gets compiled by [PyInstaller](https://www.pyinstaller.org/).

This documentation gets converted from Markdown to HTML by [PanDoc](https://pandoc.org/).

## Installation
To install _En bloc,_ you can download the executable file directly to your desktop and run it from there. On Linux, you’ll need to make it executable yet.

For a cleaner approach:

### Windows
Make a subfolder in your _Program Files_ directory and copy the executable there. You may need the administrator privileges to do that. Then right-click the executable and choose _Send To… → Create a Shortcut on the Desktop._

### Linux
Copy the binary into your local (/usr/local/bin) or user (~/bin) binary directory (or another directory on your `$PATH`), rename it to `en_bloc` (i. e. `sudo mv en_bloc-2.1.1-x86_64 /usr/local/bin/en_bloc` or `mv en_bloc-2.1.1-x86_64 ~/bin/en_bloc`) and make it executable (`sudo chmod a+x /usr/local/bin/en_bloc` or `chmod u+x ~/bin/en_bloc`, or by right-clicking on the file and setting its rights graphically). You can run it then by issuing `en_bloc` from the terminal. You can then create a symlink or a shortcut on your desktop where the _Command_ to be run should read `en_bloc`.

### From Source
Change your current directory to your local Python 3 site-packages directory. (This is `~/.local/lib/python<version>/site-packages` on Linux or `%APPDATA%\Python\Python<version>\site-packages` on Windows. If it doesn’t exist, just create it. The Python version should have the form of `3.<minor_number>`, e. g., `3.6`, on Linux; on Windows, the version appears without the dot, e. g. `36`. To get your Python’s version, issue the `python3 --version` command in the terminal.)

Once there, issue the following command from the terminal (assuming you have _[Git](https://git-scm.com/)_ installed):
```bash
git clone git@bitbucket.org:cigydd/en-bloc.git en_bloc
```

You can also clone _En bloc_ wherever you want and add the parent directory to your Python path to be able to import the `en_bloc` directory as a package.

You should install wxPython in order to be able to run _En bloc,_ e. g. `sudo apt install python3-wxgtk4.0 python3-wxgtk-media4.0 python3-wxgtk-webview4.0`.

The newly created `en_bloc` folder now includes the source code. You can test your installation by running `python3 -m en_bloc.app` from the terminal. If you see the main application window then your installation succeeded. You can create a bash or batch script containing this command to run _En bloc_ more conveniently.

## Settings
In the settings dialog, you can choose what _stone kind and colours_ you’d like to use. Just click on the small toggle buttons on the edge of the area filled with stone samples, confirm by clicking on the _OK_ button, and try what happens. You can have one stone kind selected at a time only and two colours at least.

The _Delayed Explosions_ checkbox turns a feature on or off that the stones that fall after an explosion and form a four-or-more chain explode later, after the next block touches ground. This was a bug originally but I kept it in the game as an option after my sister Daniela’s suggestion.

The _Sound Enabled_ checkbox does exactly whatʼs written on it. ☺

Happy playing,

_Cigydd [kɪgǝð]_
