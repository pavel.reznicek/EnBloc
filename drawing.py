#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Stuff for drawing stones in the En bloc game."""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"
__version__ = "0.0"

import wx
from en_bloc import constants


class StoneSrcDC(wx.MemoryDC):  # pylint: disable=too-few-public-methods

    """Provides methods for drawing stones."""

    def __init__(self):
        """The StoneSrcDC constructor."""

        wx.MemoryDC.__init__(self)
        self._stones_bmp = wx.Bitmap(constants.BMP_PATH_STONES)
        self._explosions_bmp = wx.Bitmap(constants.BMP_PATH_EXPLOSIONS)

    def draw_stone(self, the_stone, dst_dc):
        """Draw a stone on the destination DC."""
        if the_stone:
            if not the_stone.Exploding:
                self.SelectObject(self._stones_bmp)
                src_x = the_stone.Colour * constants.STONE_DIM
                src_y = the_stone.Kind * constants.STONE_DIM * 2
                mask_x = src_x
                mask_y = src_y + constants.STONE_DIM
            else:  # exploding
                self.SelectObject(self._explosions_bmp)
                src_x = the_stone.ExplosionLevel * constants.STONE_DIM
                src_y = the_stone.ExplosionKind * constants.STONE_DIM
                mask_x = src_x + 3 * constants.STONE_DIM
                mask_y = src_y
            # Draw the mask using AND
            dst_dc.Blit(
                the_stone.Pos.x * constants.STONE_DIM,
                the_stone.Pos.y * constants.STONE_DIM,
                constants.STONE_DIM, constants.STONE_DIM,
                self,
                mask_x, mask_y,
                logicalFunc=wx.AND
            )
            # Draw the stone using OR
            dst_dc.Blit(
                the_stone.Pos.x * constants.STONE_DIM,
                the_stone.Pos.y * constants.STONE_DIM,
                constants.STONE_DIM, constants.STONE_DIM,
                self,
                src_x, src_y,
                logicalFunc=wx.OR
            )
