#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""En bloc stuff that has to do with playgrounds
(not exactly: the stone blocks are defined in another module)"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import wx
from en_bloc import constants
from en_bloc import stonestuff
from en_bloc import drawing

_ = wx.GetTranslation


class StoneSpace():
    "An abstraction for a 2D-space (flat) containing stones"

    def __init__(self, size):
        self._size = size

    def is_inside(self, point):
        """Checks if a point is inside the playground"""
        return 0 <= point.x < self._size.width \
               and 0 <= point.y < self._size.height

    def fit_point(self, point):
        """Normalises a given point to fit inside the playground"""
        size = self._size
        if point.x < 0:
            point.x = 0
        if point.x >= size.width:
            point.x = size.width - 1
        if point.y < 0:
            point.y = 0
        if point.y >= size.height:
            point.y = size.height - 1

    def get_size(self):
        "The Size property getter"
        return self._size

    Size = property(get_size, doc="""The stone space’s size in the stone units ;-)
Read-only. Don’t try to change the elements, it will only break things.""")


class Playground(StoneSpace):  # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """The game field containing the stones"""

    def __init__(self, size, bkgnd_bmp, sound_player=None):
        StoneSpace.__init__(self, size)
        # Prepare the stone field / array / map
        # self._field = [[None] * size.width] * size.height # this is wrong!
        field = self._field = []
        for y_pos in range(size.height):  # pylint: disable=unused-variable
            row = []
            field.append(row)
            for x_pos in range(size.width):  # pylint: disable=unused-variable
                row.append(None)
        # Set the background bitmap
        self._bkgnd_bmp = bkgnd_bmp
        # Create a StoneSrcDC
        self._stone_src_dc = drawing.StoneSrcDC()
        # Create a playground source DC
        self._playground_src_dc = wx.MemoryDC(
            wx.Bitmap(
                size.width * constants.STONE_DIM, size.height * constants.STONE_DIM
            )
        )
        # Initialise the stone kind
        self._stone_kind = constants.SK_SPIRAL
        # Initialise the stone colours
        self._stone_colours = set(constants.STONE_COLOURS)
        # Initialise the explosion level
        self._explosion_level = 0
        self._exploding = False
        # Initialise the exploding stones list
        self._exploding_stones = []
        self._exploding_stones_collected = False
        # Initialise the remaining stones count (saved between individual block drops)
        self._remaining_stones_count = 0
        # Initialise the sound player
        self._player = sound_player
        # Initialise the Sound Enabled flag
        self._sound_enabled = False
        # Draw the contents of the playground to memory
        self.draw()

    def get_stone(self, point):
        "The stone getter"
        real_point = wx.Point(point.x, point.y)
        if self.is_inside(real_point):
            return self._field[real_point.Get()[1]][real_point.Get()[0]]  # y, x
        return None

    def set_stone(self, point, new_stone):
        """The stone setter"""
        real_point = wx.Point(point)
        self._field[real_point.y][real_point.x] = new_stone
        if isinstance(new_stone, stonestuff.Stone):
            new_stone.Pos = real_point
            new_stone.Playground = self

    def move_stone(self, old_position, new_position):
        """Moves the stone at the old_position to the new_position."""
        self.fit_point(new_position)
        old_pos_tuple = old_position.Get()
        new_pos_tuple = new_position.Get()
        # print('old position:', old_pos_tuple)
        # print('new position:', new_pos_tuple)
        if new_pos_tuple != old_pos_tuple:
            stone = self.get_stone(old_position)
            self.set_stone(old_position, None)
            self.set_stone(new_position, stone)

    def get_stone_src_dc(self):
        """The StoneSrcDC property getter"""
        return self._stone_src_dc

    StoneSrcDC = property(
        get_stone_src_dc,
        doc="The common StoneSrcDC (used as a source for stone drawing)"
    )

    def get_playground_src_dc(self):
        """The PlaygroundSrcDC property getter"""
        return self._playground_src_dc

    PlaygroundSrcDC = property(
        get_playground_src_dc,
        doc="""The playground source device context
(used as a source for the entire playground image)"""
    )

    def get_stone_kind(self):
        """The StoneKind property getter"""
        return self._stone_kind

    def set_stone_kind(self, new_kind):
        """The StoneKind property setter"""
        self._stone_kind = new_kind
        for row in self._field:
            for stone in row:
                if isinstance(stone, stonestuff.Stone):
                    stone.Kind = new_kind
        self.draw()

    StoneKind = property(
        get_stone_kind, set_stone_kind,
        doc="The kind of the contained stones"
    )

    def get_stone_colours(self):
        "The StoneColours property getter"
        return self._stone_colours

    def set_stone_colours(self, new_colours):
        "The StoneColours property setter"
        self._stone_colours = new_colours

    StoneColours = property(
        get_stone_colours, set_stone_colours,
        doc="The stone colours of the new blocks"
    )

    def draw(self):
        "Draws everything in the playground."
        self.draw_background()
        self.draw_stones()

    def draw_background(self):
        "Draws the background, erasing everything."
        self._playground_src_dc.DrawBitmap(self._bkgnd_bmp, 0, 0)

    def draw_stones(self):
        "Draws all the stones in the playground."
        for y_pos in range(self._size.height):
            for x_pos in range(self._size.width):
                stone_pos = wx.Point(x_pos, y_pos)
                stone = self.get_stone(stone_pos)
                # if stone:
                #    print(stone_pos, stone.Pos)
                self._stone_src_dc.draw_stone(stone, self._playground_src_dc)

    def get_exploding(self):
        "The Exploding property getter"
        return self._exploding

    def set_exploding(self, new_value):
        "The Exploding property setter"
        if self._exploding != new_value:
            self._exploding = new_value
            self.update_exploding()

    Exploding = property(
        get_exploding, set_exploding,
        doc="Indicates if the playground’s stones are currently performing explosions"
    )

    def update_exploding(self):
        """Updates the exploding state of the contained stones,
checking for horizontal and vertical chains."""
        # self._exploding_stones_collected = False
        # self.collect_exploding_stones()
        for stone in self._exploding_stones:
            if stone:
                stone.Exploding = self._exploding

    def get_explosion_level(self):
        """The ExplosionLevel property getter"""
        return self._explosion_level

    def set_explosion_level(self, new_level):
        """The ExplosionLevel property setter"""
        if self._explosion_level != new_level:
            self._explosion_level = new_level
            self.update_explosion_level()

    ExplosionLevel = property(
        get_explosion_level, set_explosion_level,
        doc="The current explosion level of the playground"
    )

    def get_exploding_stones(self):
        """The ExplodingStones property getter"""
        return self._exploding_stones

    ExplodingStones = property(
        get_exploding_stones,
        doc="The stones that have to explode (and have been collected)"
    )

    def collect_exploding_stones(self, discover=False):
        """Finds the all the matches for explosions if necessary"""
        if not self._exploding_stones_collected:
            if discover:
                self._exploding_stones = []
                self._collect_exploding_stones_horz()
                self._collect_exploding_stones_vert()
                self._exploding_stones_collected = True
            else:
                self._exploding_stones = []
                self._collect_stones_marked_to_expl()
                self._exploding_stones_collected = True

    def _collect_exploding_stones_horz(self):
        """Finds the horizontal matches for explosions (>=4 in a row)"""
        for y_pos in range(self._size.height):
            last_colour = None
            current_colour = None
            chain = []
            for x_pos in range(self._size.width):
                point = wx.Point(x_pos, y_pos)
                stone = self.get_stone(point)
                if stone:
                    current_colour = stone.Colour
                    if current_colour != last_colour:
                        chain = []
                    chain.append(stone)
                    # If we found a 4-or-more chain
                    if len(chain) >= 4:
                        for chain_stone in chain:
                            self._exploding_stones.append(chain_stone)
                else:
                    current_colour = None
                    chain = []
                last_colour = current_colour

    def _collect_exploding_stones_vert(self):
        "Finds the vertical matches for explosions (>=4 in a column)"
        for x_pos in range(self._size.width):
            last_colour = None
            current_colour = None
            chain = []
            for y_pos in range(self._size.height):
                point = wx.Point(x_pos, y_pos)
                stone = self.get_stone(point)
                if stone:
                    current_colour = stone.Colour
                    if current_colour != last_colour:
                        chain = []
                    chain.append(stone)
                    # If we found a 4-or-more chain
                    if len(chain) >= 4:
                        for chain_stone in chain:
                            self._exploding_stones.append(chain_stone)
                else:
                    current_colour = None
                    chain = []
                last_colour = current_colour

    def _collect_stones_marked_to_expl(self):
        """Collects all stones marked for explosion (.Exploding = True)"""
        for x_pos in range(self._size.width):
            for y_pos in range(self._size.height):
                point = wx.Point(x_pos, y_pos)
                stone = self.get_stone(point)
                if stone:
                    if stone.Exploding:
                        self._exploding_stones.append(stone)

    @property
    def UnexplodingStones(self):
        """All stones that arenʼt marked for explosion.
Collects all stones not marked for explosion (.Exploding == False)."""
        result = []
        for x_pos in range(self._size.width):
            for y_pos in range(self._size.height):
                point = wx.Point(x_pos, y_pos)
                stone = self.get_stone(point)
                if stone:
                    if not stone.Exploding:
                        result.append(stone)
        return result

    @property
    def UnexplodingStonesCount(self):
        """Count of the stones that arenʼt marked for explosion."""
        stones = self.UnexplodingStones
        return len(stones)

    def update_explosion_level(self):
        """Updates the explosion level of the contained stones
to the explosion level of the playground"""
        for y_pos in range(self._size.height):
            for x_pos in range(self._size.width):
                point = wx.Point(x_pos, y_pos)
                stone = self.get_stone(point)
                if isinstance(stone, stonestuff.Stone) and stone.Exploding:
                    stone.ExplosionLevel = self._explosion_level

    def explode(self, game_mode):
        """Performs all explosions on contained stones."""
        if not self.Exploding:
            self._exploding_stones_collected = False
            self.collect_exploding_stones(discover=True)
            if self._exploding_stones:
                print(_("Starting explosions…"))
                self.Exploding = True
                self.ExplosionLevel = 0
                self.play_sound()
            if game_mode == constants.GM_DETONATOR:
                return len(self._exploding_stones)
            if game_mode == constants.GM_CASTLE:
                # Save the last remaining stones count into a local variable
                last_remaining_stones_count = self._remaining_stones_count
                # Find the new remaining stones
                self._remaining_stones_count = self.UnexplodingStonesCount
                # Compute the difference
                result = self._remaining_stones_count - last_remaining_stones_count
                # Return the result
                return result
            return 0 # should never happen
        # Exploding
        if self.ExplosionLevel < 2:
            if self._exploding_stones:
                self.ExplosionLevel += 1
                print(_("Raising explosion level to %s…") % self.ExplosionLevel)
        else:  # >= 2
            self._exploding_stones_collected = False
            self.collect_exploding_stones()
            while self._exploding_stones:
                print(_("Falling…"))
                self.fall()
                self._exploding_stones_collected = False
                self.collect_exploding_stones()
            self.Exploding = False
            self.ExplosionLevel = 0
            print(_("Explosions finished."))
        return 0

    def fall(self):
        """Makes all the exploded stones vanish
and the stones upon them fall."""
        if self.ExplosionLevel == 2:
            for stone in self._exploding_stones:
                pos = stone.Pos
                x_pos, y_pos = pos
                # Let the stone vanish
                self.set_stone(pos, None)
                self._exploding_stones.remove(stone)
                # Let the column fall
                for column_pos in range(y_pos, -1, -1):
                    target_point = wx.Point(x_pos, column_pos)
                    source_point = wx.Point(x_pos, column_pos - 1)
                    falling_stone = self.get_stone(source_point)
                    self.set_stone(target_point, falling_stone)

    def can_explode(self):
        """Indicates if the playground contains chains that can explode."""
        self._exploding_stones_collected = False
        self.collect_exploding_stones(discover=True)
        return bool(self._exploding_stones)

    def clear(self):
        """Clears the playground from all contained stones."""
        for y_pos in range(self._size.height):
            for x_pos in range(self._size.width):
                point = wx.Point(x_pos, y_pos)
                self.set_stone(point, None)

    @property
    def DefaultBlockPosition(self):
        """Gets the default position of newly placed blocks."""
        return wx.Point(self.Size.width // 2, 0)

    @property
    def SoundEnabled(self):
        """The Sound Enabled flag"""
        return self._sound_enabled

    @SoundEnabled.setter
    def SoundEnabled(self, enabled):
        """The SoundEnabled property setter"""
        self._sound_enabled = enabled

    def play_sound(self):
        """Plays the explosion sound."""
        if self._sound_enabled:
            if self._player:
                self._player.play()
