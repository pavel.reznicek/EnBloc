#!/usr/bin/env python3

"""A simple sound player.
Currently unused because pydub requires ffmpeg and has json issues on Debian Buster."""

import threading
from pydub import AudioSegment
import pydub.playback

class PlayerThread(threading.Thread):
    """Used to asynchronously play sounds"""
    def __init__(self, sound_object):
        threading.Thread.__init__(self, name="En bloc player thread")
        self._sound = sound_object

    @property
    def Sound(self):
        """The core sound object"""
        return self._sound

    def run(self):
        pydub.playback.play(self._sound)

class MP3Player():
    """A simple class that loads an MP3 file upon creation
and plays it on demand."""
    def __init__(self, sound_file_name):
        self._sound = AudioSegment.from_mp3(sound_file_name)  # from_wav may be used for wave files

    @property
    def Sound(self):
        """The core sound object"""
        return self._sound

    def play(self):
        """Plays the sound loaded upon creation"""
        thread = PlayerThread(self._sound)
        thread.start()
