#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""En bloc.
A game inspired by an old freeware game for Windows called Block Game
(which in turn is probably inspired by the famous Tetris game)"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import wx
from en_bloc import constants
from en_bloc import playgroundstuff
from en_bloc import blockstuff
from en_bloc import statuspanel
from en_bloc import settingsdialog
from en_bloc import helpdialog
from en_bloc import gamemodedialog
from en_bloc import sound
from en_bloc import i18nbase

_ = wx.GetTranslation

class EnBlocApp(i18nbase.I18nBaseApp):  # pylint: disable=too-many-ancestors
    """The En bloc main application class.

    Call the MainLoop method to start it."""

    def __init__(self):
        """EnBlocApp() -> EnBlocApp."""
        super().__init__(redirect=False)

        frm = self.frm = Form()
        self.SetTopWindow(frm)
        frm.Show()


class Form(wx.Frame):  # pylint: disable=too-many-ancestors,too-many-instance-attributes,too-many-public-methods
    """The main En bloc form class."""

    def __init__(self, parent=None):  # pylint: disable=too-many-locals,too-many-statements
        wx.Frame.__init__(self, parent=parent, title="En bloc",
                          style=wx.DEFAULT_FRAME_STYLE  # | wx.WANTS_CHARS)
                          & ~ (wx.MAXIMIZE_BOX | wx.RESIZE_BORDER))

        settings_dlg = self.settings_dlg = settingsdialog.SettingsDialog(self)
        settings = self.settings = settings_dlg.Settings
        stone_kind = settings.StoneKind
        stone_colours = settings.StoneColours
        sound_enabled = settings.SoundEnabled

        self.help_dlg = helpdialog.HelpDialog(self)

        self.game_mode_dlg = gamemodedialog.GameModeDlg(self)

        self._game_mode = constants.GM_DETONATOR

        bgnd_bmp = self.bgnd_bmp = wx.Bitmap(constants.BMP_PATH_BACKGROUND)

        szr_vertical = wx.BoxSizer(wx.VERTICAL)

        szr_horizontal = wx.BoxSizer(wx.HORIZONTAL)
        pan = self.pan = wx.Panel(self, style=wx.WANTS_CHARS | wx.BORDER_RAISED)
        szr_horizontal.Add(pan, 1, wx.GROW)

        wdth, hght = bgnd_bmp.Size  # pylint: disable=unpacking-non-sequence
        border_horz = pan.Size.width - pan.ClientSize.width  # pylint: disable=no-member
        border_vert = pan.Size.height - pan.ClientSize.height  # pylint: disable=no-member
        wdth -= wdth % constants.STONE_DIM - border_horz
        hght -= hght % constants.STONE_DIM - border_vert

        pan_status = self.pan_status = statuspanel.StatusPanel(
            self, stone_kind=stone_kind, stone_colours=stone_colours
        )
        szr_horizontal.Add(pan_status, 0, wx.GROW)

        wdth += pan_status.Size.Width  # pylint: disable=no-member

        toolbar = self.ToolBar = wx.ToolBar(self)

        tool_new_game = self.tool_new_game = toolbar.AddTool(
            101, _("New Game"), wx.Bitmap(constants.BMP_PATH_NEW_GAME)
        )
        toolbar.Bind(wx.EVT_TOOL, self.on_new_game_tool, tool_new_game)

        toolbar.AddSeparator()

        tool_settings = self.tool_settings = toolbar.AddTool(
            102, _("Settings"), wx.Bitmap(constants.BMP_PATH_SETTINGS)
        )
        toolbar.Bind(wx.EVT_TOOL, self.on_settings_tool, tool_settings)

        toolbar.AddSeparator()

        tool_help = self.tool_help = toolbar.AddTool(
            103, _("Help"), wx.Bitmap(constants.BMP_PATH_HELP)
        )
        toolbar.Bind(wx.EVT_TOOL, self.on_help_tool, tool_help)

        toolbar.AddSeparator()

        tool_close = self.tool_close = toolbar.AddTool(
            104, _("Close"), wx.Bitmap(constants.BMP_PATH_CLOSE)
        )
        toolbar.Bind(wx.EVT_TOOL, self.on_close_tool, tool_close)

        toolbar.Realize()

        szr_vertical.Add(szr_horizontal, 1, wx.GROW)

        self.Sizer = szr_vertical

        self.SetClientSize((wdth, hght))
        self.Bind(wx.EVT_SHOW, self.on_show)

        maximum = self.maximum = self.pan.GetClientSize()
        size_in_stones = self.size_in_stones = wx.Size(
            maximum.width // constants.STONE_DIM,
            maximum.height // constants.STONE_DIM
        )

        player = self.player = sound.SoundPlayer(constants.SND_FILE_EXPLOSION)

        pgnd = self.pgnd = playgroundstuff.Playground(size_in_stones, bgnd_bmp, player)
        pgnd.StoneKind = stone_kind
        pgnd.StoneColours = stone_colours
        pgnd.SoundEnabled = sound_enabled
        self.block = blockstuff.VerticalTetriBlock(pgnd)
        pgnd.draw()

        tmr_exploder = self.tmr_exploder = wx.Timer(self)

        pan.Bind(wx.EVT_KEY_DOWN, self.on_key_down)
        pan.Bind(wx.EVT_PAINT, self.on_paint)
        # pan.Bind(wx.EVT_ERASE_BACKGROUND, self.on_erase_background)
        self.Bind(wx.EVT_TIMER, self.on_timer, tmr_exploder)
        self.Bind(wx.EVT_CLOSE, self.on_close)

        # self.Show()

    def on_key_down(self, evt):
        """The key down event handler"""
        key = evt.GetKeyCode()
        if key in (wx.WXK_LEFT, ord('A')):
            self.move(-1, 0)
        elif key in (wx.WXK_RIGHT, ord('D')):
            self.move(1, 0)
        elif key in (wx.WXK_UP, ord('W')):
            # self.move(0, -1)
            self.cycle()
        elif key in (wx.WXK_DOWN, ord('S')):
            self.move(0, 1)

    def move(self, xshift, yshift):
        """Changes the falling block’s position"""
        if self.CanMove:
            # Localise instance attributes
            block = self.block
            if block:
                pgnd = self.pgnd
                pan = self.pan
                # Move the block
                block.move(xshift, yshift)
                # If we got to the bottom:
                if block.IsOnBottom:
                    # Turn on the explosion timer
                    if not self.tmr_exploder.IsRunning():
                        self.tmr_exploder.Start(constants.EXPLOSION_INTERVAL)
                # Draw the playground in the memory
                pgnd.draw()
                # Refresh the panel and draw the playground on it
                pan.Refresh()

    def cycle(self):
        """Cycles the stones in the moving block by one upwards."""
        block = self.block
        if block:
            pgnd = self.pgnd
            pan = self.pan
            block.cycle()
            pgnd.draw()
            pan.Refresh()

    def explode(self):
        """Explodes the match making stones"""
        pgnd = self.pgnd
        pan = self.pan
        points = pgnd.explode(self.GameMode)
        self.Score += points ** 2  # exponential rewarding
        # If the explosion ended already,
        if not pgnd.Exploding:
            # If nothing remains that can explode
            # or delayed explosions are enabled
            if (not pgnd.can_explode()) or self.settings.DelayedExplosions:
                # Stop the explosion timer
                self.tmr_exploder.Stop()
                # load a new block
                loaded = self.load_new_block()
                # if self.block.IsOnBottom:
                if not loaded:
                    self.pgnd.draw()
                    self.pan.Refresh()
                    self.block = None

                    msg = self.game_ended_msg()
                    wx.MessageBox(msg)

                    self.save_high_score_if_beaten()
            # else keep the timer running
        pgnd.draw()
        pan.Refresh()

    def beaten_condition(self):
        """The sole condition if the current score is
better than the high score"""
        return self.Score > self.HighScore

    def final_beaten_text(self):
        """The final resulting message part saying
if the high score has been beaten, or if not,
an empty string."""
        condition = self.beaten_condition()
        if condition:
            beaten = _("""
You have beaten the high score!""")
        else:
            beaten = ""
        return beaten

    def game_ended_msg(self):
        """The message shown on the end of the game."""
        score = self.Score
        beaten = self.final_beaten_text()
        highscore = self.HighScore
        result = _("""The game has ended.%s
Your score is %s.
High score is %s.""") % (beaten, score, highscore)
        return result

    def save_high_score_if_beaten(self):
        """Saves the current score as a new high score
if the high score has been actually beaten."""
        condition = self.beaten_condition()
        if condition:
            score = self.Score
            self.HighScore = score

    def load_new_block(self):
        """Loads a new block into the playground"""
        pan_status = self.pan_status
        block = self.block = pan_status.Block
        pgnd = self.pgnd
        # Change the playground from the status panel new block playground
        # to the game playground
        try:
            block.Playground = pgnd
            pan_status.create_new_block()  # create a new block in queue
            return True
        except blockstuff.FieldOccupiedError:
            return False

    def new_game(self):
        """Starts a new game, asking for its mode."""
        mode = self.game_mode_dlg.ShowModal()
        self.GameMode = mode
        self.Score = 0
        self.pgnd.clear()
        # The last block uses the old colour settings and, in addition,
        # may be damaged from the end of the previous game,
        # so letʼs create a new one:
        self.pan_status.create_new_block()
        # Try to shift it into the main playground:
        if self.load_new_block():
            self.pgnd.draw()
            self.pan.Refresh()
        else:
            raise RuntimeError(
                _(
                    "Could not load a new block. "
                    "The position is already occupied."
                )
            )

    def get_can_move(self):
        """The CanMove property getter"""
        # return not self.tmr_exploder.IsRunning()
        return not (self.pgnd.Exploding or self.tmr_exploder.IsRunning())

    CanMove = property(get_can_move, doc="Indicates if the free block can be moved.")

    @property
    def GameMode(self):
        """The mode of the current game"""
        return self._game_mode

    @GameMode.setter
    def GameMode(self, new_mode):
        """The GameMode property setter"""
        # self.pan_status.GameMode = new_mode
        self._game_mode = new_mode

    @property
    def HighScore(self):
        """The high score,
depending on the game mode."""
        if self.GameMode == constants.GM_DETONATOR:
            return self.settings.DetonatorHighScore
        if self.GameMode == constants.GM_CASTLE:
            return self.settings.CastleHighScore
        return None

    @HighScore.setter
    def HighScore(self, new_score):
        """The HighScore property setter"""
        if self.GameMode == constants.GM_DETONATOR:
            self.settings.DetonatorHighScore = new_score
        elif self.GameMode == constants.GM_CASTLE:
            self.settings.CastleHighScore = new_score

    @property
    def Score(self):
        """The current game score"""
        return self.pan_status.Score

    @Score.setter
    def Score(self, new_score):
        """The Score property setter"""
        self.pan_status.Score = new_score

    def on_show(self, evt):
        """The show event handler.
Called if the window gets shown."""
        if evt.IsShown():
            self.new_game()
            pan = self.pan
            pan.SetFocus()
            evt.Skip()

    def on_paint(self, evt):
        """The paint event handler.
Called if the game area gets repainted."""
        bpdc = wx.BufferedPaintDC(evt.EventObject)
        pgnd = self.pgnd
        pgnd_dc = pgnd.PlaygroundSrcDC
        pgnd_size = pgnd.Size
        pgnd_wdth = pgnd_size.Width * constants.STONE_DIM
        pgnd_hght = pgnd_size.Height * constants.STONE_DIM
        bpdc.Blit(0, 0, pgnd_wdth, pgnd_hght, pgnd_dc, 0, 0)

    # def on_erase_background(self, evt):  # pylint: disable=no-self-use
    #     """Prevents the background from being erased, causing flickering on Windows.
    #     @param evt: wx.EraseBackgroundEvent - calling event
    #     """
    #     evt.Skip()

    def on_timer(self, evt):
        """The timer event handler.
        @param evt: the calling event
        """
        print(_('The timer has elapsed.'))
        self.explode()
        evt.Skip()

    def on_new_game_tool(self, evt):
        """The tool_new_game press event handler."""
        if self.block:
            response = wx.MessageBox(_("""A game is already running.
Would you like to start a new one?"""), self.Title, wx.YES_NO)
            start_new_game = (response == wx.YES)
        else:
            start_new_game = True
        if start_new_game:
            self.new_game()
        evt.Skip()

    def on_settings_tool(self, evt):
        """The tool_settings press event handler."""
        modal_result = self.settings_dlg.ShowModal()
        if modal_result == wx.ID_OK:
            self.apply_settings()
            self.new_game()
        evt.Skip()

    def on_help_tool(self, evt):
        """The tool_help press event handler."""
        self.help_dlg.ShowModal()
        evt.Skip()

    def on_close_tool(self, evt):
        """The tool_close press event handler."""
        self.Close()
        evt.Skip()

    def on_close(self, evt):
        """Handles the frame’s close event."""
        if evt.CanVeto():
            response = wx.MessageBox(
                _("Are you sure you want to quit %s?") % self.Title,
                self.Title,
                style=wx.OK | wx.CANCEL | wx.CENTRE
            )
            if response != wx.OK:
                evt.Veto()
                return
        evt.Skip()

    def apply_settings(self):
        """Applies the settings after changes in the settings dialog"""
        settings = self.settings
        pgnd = self.pgnd
        pan_status = self.pan_status
        stone_kind = settings.StoneKind
        stone_colours = settings.StoneColours
        sound_enabled = settings.SoundEnabled
        pan_status.StoneKind = stone_kind
        pan_status.StoneColours = stone_colours
        pgnd.StoneKind = stone_kind
        pgnd.StoneColours = stone_colours
        pgnd.SoundEnabled = sound_enabled
        self.Refresh()


if __name__ == '__main__':
    APP = EnBlocApp()
    APP.MainLoop()
