# En bloc
Hra s padajícími bloky

_En bloc_ chce být jednoduchou dvojrozměrnou hrou s padajícími bloky, z nichž každý sestává ze čtyř čtvercových kamenů dvou různých barev. Hráč má řídit padající blok pomocí kláves směrových šipek (nebo A, W, S a D). Když blok dopadne na dno herní plochy, je zde upuštěn, začíná padat nový blok a hráč má řídit tento nový blok.

Jak bloky padají na dno, staví zeď. Pokud se v této zdi při dopadu nového bloku spojí čtyři nebo více čtvercových kamenů stejné barvy ve vodorovné či svislé řadě, vybuchnou a zmizí. Kameny, které leží na nich, spadnou a zaujmou jejich místo.

## Autoři a přispěvatelé
Kód: Pavel Řezníček neboli Cigydd [kɪgǝð] – <pavel.reznicek@evangnet.cz>. Viz můj ostatní kód [zde](https://bitbucket.org/cigydd/).

Grafika: Marta Vostrá – můžete navštívit stránku [www.mravkolev.cz](https://www.mravkolev.cz/), chcete-li vidět příklady její tvorby.

Testování: Daniela Běťáková.

## Inspirace
Tato hra je silně inspirována starší volně šiřitelnou hrou pro 32-bitová Windows zvanou _Watson’s Block Game (Watsonova hra s bloky)_, kterou napsal _Johannes Wallroth,_ viz [www.programming.de](https://www.programming.de/index.php/archive#games-fun-stuff).

## Užité nástroje
_En bloc_ je napsán v [Pythonu 3](https://www.python.org/) a jeho knihovně grafického uživatelského rozhraní [wxPython](https://wxpython.org/).

Zvuk je přehráván pomocí pythonové knihovny [PySDL2](https://pypi.org/project/PySDL2/), což je obal [knihovny SDL2](https://www.libsdl.org/).
Knihovny SDL2 a SDL2_mixer jsou dodávány ve spustitelném souboru hry.

Spustitelný soubor je sestavován [PyInstallerem](https://www.pyinstaller.org/).

Tato dokumentace je převáděna z Markdownu do jazyka HTML [PanDocem](https://pandoc.org/).

## Installace
Chcete-li installovat _En bloc,_ můžete si stáhnout spustitelný soubor přímo na svou plochu a spustit jej odtud. Na Linuxu budete ještě potřebovat učinit jej spustitelným.

Pro čistší přístup:

### Windows

Vytvořte podaddressář ve svém addressáři _Program Files_ a zkopírujte tam spustitelný soubor. Zřejmě k tomu budete potřebovat práva administrátora. Pak klepněte pravým tlačítkem na spustitelný soubor a vyberte _Odeslat… → Vytvořit zástupce (Plocha)._

### Linux
Zkopírujte binární soubor do svého lokálního (/usr/local/bin) nebo uživatelského (~/bin) addressáře pro binární soubory (nebo jiného addressáře na své cestě (`$PATH`)), přejmenujte jej na `en_bloc` (např. `sudo mv en_bloc-2.1.1-x86_64 /usr/local/bin/en_bloc` nebo `mv en_bloc-2.1.1-x86_64 ~/bin/en_bloc`) a učiňte jej spustitelným (`sudo chmod a+x /usr/local/bin/en_bloc` nebo `chmod u+x ~/bin/en_bloc` nebo klepnutím pravým tlačítkem na soubor a grafickým nastavením jeho práv). Pak jej můžete spustit zadáním příkazu `en_bloc` v terminálu. Pak můžete vytvořit na ploše symbolický odkaz nebo zástupce, kde _příkaz_ ke spuštění by měl znít `en_bloc`.

### Ze zdroje

Změňte svůj pracovní addressář na svůj uživatelský addressář balíčků třetích stran (site-packages) Pythonu 3. (To je v Linuxu `~/.local/lib/python<verse>/site-packages` nebo ve Windows `%APPDATA%\Python\Python<verse>\site-packages`. Pokud neexistuje, prostě jej vytvořte. Verse Pythonu by měla mít v Linuxu tvar `3.<drobnější_verse>`, např. `3.6`; ve Windows se verse objevuje bez tečky, např. `36`. Chcete-li zjistit versi svého Pythonu, zadejte v terminálu příkaz `python3 --version`.)

Až tam budete, zadejte v terminálu tento příkaz (předpokládám, že máte installovaný _[Git](https://git-scm.com/)_):
```bash
git clone git@bitbucket.org:cigydd/en-bloc.git en_bloc
```
Můžete také klonovat _En bloc_, kamkoliv chcete, a přidat rodičovský addressář do své pythonové cesty, abyste mohli importovat addressář `en_bloc` jako balíček.

Měli byste installovat wxPython, abyste mohli spouštět _En bloc,_ např. `sudo apt install python3-wxgtk4.0 python3-wxgtk-media4.0 python3-wxgtk-webview4.0`.

Nově vytvořený addressář `en_bloc` nyní obsahuje zdrojový kód. Můžete vyzkoušet svou installaci spuštěním příkazu `python3 -m en_bloc.app` z terminálu. Pokud vidíte hlavní okno applikace, pak vaše installace uspěla. Můžete vytvořit bashový nebo dávkový skript obsahující tento příkaz, abyste mohli spouštět _En bloc_ pohodlněji.

## Nastavení

V dialogovém okně nastavení si můžete vybrat, jaký _druh a barvu kamenů_ byste chtěli používat. Zkrátka klepejte na malé přepínače na okraji oblasti naplněné vzorky kamenů, potvrďte klepnutím na tlačítko _Budiž (OK)_ a vyzkoušejte, co se stane. Můžete mít vybrán pouze jeden druh kamenů a nejméně dvě barvy.

Zaškrtávátko _Opožděné výbuchy_ zapíná nebo vypíná funkci, že kameny, které spadnou po výbuchu a zformují řetězec čtyř nebo více, vybuchnou později, až poté, co se další blok dotkne dna. Toto byla původně chyba, ale ponechal jsem to ve hře jako možnost dle doporučení své sestry Daniely.

Zaškrtávátko _Zvuk povolen_ dělá přesně to, co je na něm napsáno. ☺

Příjemné hraní přeje

_Cigydd [kɪgǝð]_
