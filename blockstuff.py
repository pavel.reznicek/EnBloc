#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""En bloc stuff that has to do with stone blocks."""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import random
import wx
from en_bloc.lists import rotate_list
from en_bloc import stonestuff
from en_bloc import playgroundstuff

_ = wx.GetTranslation

class FieldOccupiedError(RuntimeError):
    """An exception class that should be raised if a stone position is already occupied
and a stone or a block canʼt be moved there."""


class Block(playgroundstuff.StoneSpace):
    """A class representing a compact moving stone block inside a playground"""

    def __init__(self, size, playground):
        playgroundstuff.StoneSpace.__init__(self, size)
        self._playground = playground
        self._pos = playground.DefaultBlockPosition
        self._stones = []

    def get_playground(self):
        """The Playground property getter."""
        return self._playground

    def set_playground(self, new_pgnd):
        """The Playground property setter."""
        old_pgnd = self._playground
        if old_pgnd != new_pgnd:
            self.detach()
            self._playground = new_pgnd
            for stone in self._stones:
                stone.Playground = new_pgnd
            self.attach()

    Playground = property(
        get_playground,
        set_playground,
        doc="The playground the block is placed in."
    )

    def get_pos(self):
        """The Pos property getter."""
        return self._pos
    def set_pos(self, new_pos):
        """The Pos property setter."""
        self._pos = new_pos

    Pos = property(get_pos, set_pos, doc="The blockʼs position inside the playground.")

#     def get_stone(self, point):
#         """The stone getter.
# The point argument is relative to the blockʼs Pos."""
#         real_point = wx.Point(point)
#         abs_pos = wx.Point(
#             real_point.Get()[0] + self._pos.Get()[0],
#             real_point.Get()[1] + self._pos.Get()[1]
#         )
#         if self.is_inside(real_point):
#             return self._playground.get_stone(abs_pos)
#         return None
#
#     def set_stone(self, point, new_stone):
#         """The stone setter.
# The point argument is relative to the block’s Pos."""
#         real_point = wx.Point(point)
#         new_pos = wx.Point(
#             real_point.Get()[0] + self._pos.Get()[0],
#             real_point.Get()[1] + self._pos.Get()[1]
#         )
#         self._playground.set_stone(new_pos, new_stone)

    def can_move(self, xshift, yshift):
        """Indicates if the block can move by xshift columns and yshift rows."""
        result = True
        for stone in self._stones:
            new_pos = wx.Point(stone.Pos)
            new_pos.x += xshift
            new_pos.y += yshift
            new_pos_stone = self._playground.get_stone(new_pos)
            is_inside = self._playground.is_inside(new_pos)
            result = result and (not new_pos_stone) and is_inside
        return result

    def detach(self):
        """Detaches the block’s stones from the playground."""
        for stone in self._stones:
            self._playground.set_stone(stone.Pos, None)

    def attach(self):
        """Attaches the block’s stones to the playground."""
        for stone in self._stones:
            self._playground.set_stone(stone.Pos, stone)

    def move(self, xshift, yshift):
        """Moves the block by xshift columns and yshift rows."""
        self.detach()
        if self.can_move(xshift, yshift):
            for stone in self._stones:
                new_pos = wx.Point(stone.Pos)
                new_pos.x += xshift
                new_pos.y += yshift
                self._playground.set_stone(new_pos, stone)
            self._pos.x += xshift
            self._pos.y += yshift
            result = True
        else:
            result = False
        self.attach()
        return result

    def can_drop(self, target_playground, target_position=None):
        """Indicates that the block can drop onto the target playground
at the target position"""
        if target_position is None:
            target_position = target_playground.DefaultBlockPosition
        for rel_pos in self.StoneRelativePositions:
            abs_pos = target_position + rel_pos
            target_field = target_playground.get_stone(abs_pos)
            if target_field:  # that field is already occupied
                return False
        return True  # all fields were free


    def get_relative_position(self, absolute_position):
        """Converts an absolute position in a playground to a position
relative to the blockʼs position"""
        return absolute_position - self._pos

    def get_stone_relative_position(self, stone):
        """Calculates a given stoneʼs position relative to the blockʼs position"""
        return stone.Pos - self._pos

    @property
    def StoneRelativePositions(self):
        """The relative positions for all the stones, indexed by the order in the Stones list"""
        result = [self.get_stone_relative_position(stone) for stone in self._stones]
        return result

    def get_is_on_bottom(self):
        """The IsOnBottom property getter."""
        self.detach()
        result = not self.can_move(0, 1)
        self.attach()
        return result

    IsOnBottom = property(
        get_is_on_bottom,
        doc="Indicates if the block can’t move downwards."
    )

    def get_is_on_top(self):
        """The IsOnTop property getter."""
        self.detach()
        result = not self.can_move(0, -1)
        self.attach()
        return result

    IsOnTop = property(
        get_is_on_top,
        doc="Indicates if the block can’t move upwards."
    )


class VerticalTetriBlock(Block):
    """A block consisting of four stones arranged in a single column."""

    def __init__(self, playground):
        size = wx.Size(1, 4)
        Block.__init__(self, size, playground)

        block_stone_colours = mix_vert_tetriblock_colours(playground.StoneColours)

        # Create the stones and set them on the playground
        for y_pos in range(size.height):
            stone = stonestuff.Stone()
            stone.Colour = block_stone_colours[y_pos]
            stone.Kind = playground.StoneKind
            stone_pos = self._playground.DefaultBlockPosition
            stone_pos.y += y_pos
            self._stones.append(stone)
            self._playground.set_stone(stone_pos, stone)
            # print(stone.Pos)

    @property
    def Playground(self):
        """The playground the block is placed in."""
        return Block.Playground

    @Playground.setter
    def Playground(self, new_pgnd):
        """The Playground property setter."""
        # Cycle through the stones and set them new coordinates before the block gets attached
        old_pgnd = self._playground
        if old_pgnd != new_pgnd:
            if self.can_drop(new_pgnd):
                self.detach()
                self._playground = new_pgnd
                for y in range(self.Size.height):
                    stone = self._stones[y]
                    new_pos = wx.Point(new_pgnd.DefaultBlockPosition)
                    new_pos.y += y
                    stone.Pos = new_pos
                    stone.Playground = new_pgnd
                self.attach()
            else:
                raise FieldOccupiedError(
                    _(
                        "Thereʼs already a stone there in the target playground "
                        "on the target position."
                    )
                )

    def cycle(self):
        """Cycles the stones in the block by one upwards (actually only the colours)."""
        colour_sequence = [stone.Colour for stone in self._stones]
        new_colour_sequence = rotate_list(colour_sequence, -1)
        for i in range(len(self._stones)):
            stone = self._stones[i]
            colour = new_colour_sequence[i]
            stone.Colour = colour


def mix_vert_tetriblock_colours(selected_colours):
    """Creates a colour sequence (list) for a vertical tetriblock."""
    # Copy the stone colour constant list,
    colours = list(selected_colours)
    # shuffle them,
    random.shuffle(colours)
    # and pick the first two colours
    block_colours = colours[:2]
    # Find a suitable color combination
    # (recursively filter out one-coloured chains)
    block_stone_colours = []
    colours_ok = False
    while not colours_ok:
        if hasattr(random, 'choices'):
            block_stone_colours = random.choices(block_colours, k=4)  # pylint: disable=no-member
        else:
            block_stone_colours = [None] * 4
            for i in range(4):
                block_stone_colour = random.choice(block_colours)
                block_stone_colours[i] = block_stone_colour
        colours_ok = not (
            block_stone_colours[0] == block_stone_colours[1] ==
            block_stone_colours[2] == block_stone_colours[3]
        )
    return block_stone_colours
