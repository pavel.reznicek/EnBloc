#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The En bloc settings stored in a configuration file"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import wx
from en_bloc import constants

PATH_DELAYED_EXPLOSIONS = "/DelayedExplosions"
PATH_STONE_KIND = "/StoneKind"
PATH_STONE_COLOUR = "/StoneColour"
PATH_CASTLE_HIGH_SCORE = "/CastleHighScore"
PATH_DETONATOR_HIGH_SCORE = "/DetonatorHighScore"
PATH_SOUND_ENABLED = "/SoundEnabled"

STONE_COLOUR_NAMES = [
    "Red",
    "Green",
    "Blue",
    "Grey",
    "Yellow",
    "White"
]


class Settings(wx.FileConfig):
    """The En bloc settings class"""

    def __init__(self):
        super().__init__(
            appName="En bloc", vendorName="Cigydd"
        )

    @property
    def DelayedExplosions(self):
        """The DelayedExplosions property getter"""
        return self.ReadBool(PATH_DELAYED_EXPLOSIONS, True)

    @DelayedExplosions.setter
    def DelayedExplosions(self, value):
        """The DelayedExplosions property setter"""
        self.WriteBool(PATH_DELAYED_EXPLOSIONS, value)

    @property
    def StoneKind(self):
        """The StoneKind property getter"""
        result = self.ReadInt(PATH_STONE_KIND, constants.SK_SPIRAL)
        if result > len(constants.STONE_KINDS) - 1:
            result = constants.SK_SPIRAL
        return result

    @StoneKind.setter
    def StoneKind(self, value):
        """The StoneKind property setter"""
        self.WriteInt(PATH_STONE_KIND, value)

    def get_stone_colour_use(self, colour):
        """Get a stone colour use flag"""
        return self.ReadBool(PATH_STONE_COLOUR + STONE_COLOUR_NAMES[colour], True)

    def set_stone_colour_use(self, colour, new_use):
        """Set a stone colour use flag"""
        self.WriteBool(PATH_STONE_COLOUR + STONE_COLOUR_NAMES[colour], new_use)

    @property
    def StoneColours(self):
        """The stone colours used in the playground"""
        result = set()
        for colour in constants.STONE_COLOURS:
            colour_use = self.get_stone_colour_use(colour)
            if colour_use:
                result.add(colour)
        return result

    @StoneColours.setter
    def StoneColours(self, new_colours):
        """The StoneColours property setter"""
        for colour in constants.STONE_COLOURS:
            colour_use = colour in new_colours
            self.set_stone_colour_use(colour, colour_use)

    @property
    def DetonatorHighScore(self):
        """The high score reached in the game so far in the detonator mode"""
        return self.ReadLong(PATH_DETONATOR_HIGH_SCORE, 0)

    @DetonatorHighScore.setter
    def DetonatorHighScore(self, new_highscore):
        """The DetonatorHighScore property setter"""
        self.WriteInt(PATH_DETONATOR_HIGH_SCORE, new_highscore)
        self.Flush()

    @property
    def CastleHighScore(self):
        """The high score reached in the game so far in the game mode"""
        return self.ReadLong(PATH_CASTLE_HIGH_SCORE, 0)

    @CastleHighScore.setter
    def CastleHighScore(self, new_highscore):
        """The CastleHighScore property setter"""
        self.WriteInt(PATH_CASTLE_HIGH_SCORE, new_highscore)
        self.Flush()

    @property
    def SoundEnabled(self):
        """The Sound Enabled flag"""
        return self.ReadBool(PATH_SOUND_ENABLED, True)

    @SoundEnabled.setter
    def SoundEnabled(self, enabled):
        """The SoundEnabled property setter"""
        self.WriteBool(PATH_SOUND_ENABLED, enabled)
        self.Flush()
