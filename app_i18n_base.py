# -*- coding: utf-8 -*-
"""
Author:   Werner F. Bruhin
Purpose:  how to I18N enable an application

Inspired by the I18N wxPython demo and the Internationalization page on
the wxPython wiki.
"""

import builtins  # pylint: disable=wrong-import-position,wrong-import-order
import sys
#import os
import locale

import wx

from wx.lib.mixins.inspection import InspectionMixin

from en_bloc import constants

# Install a custom displayhook to keep Python from setting the global
# _ (underscore) to the value of the last evaluated expression.  If
# we don't do this, our mapping of _ to gettext can get overwritten.
# This is useful/needed in interactive debugging with PyShell.


def _displayhook(obj):
    if obj is not None:
        print(repr(obj))


# add translation macro to builtin similar to what gettext does
builtins.__dict__['_'] = wx.GetTranslation


class I18nBaseApp(wx.App, InspectionMixin):  # pylint: disable=too-many-ancestors
    """The i18n base for an application"""
    def OnInit(self):  # pylint: disable=invalid-name
        """Called on application initialisation"""
        self.Init()  # InspectionMixin
        # work around for Python stealing "_"
        sys.displayhook = _displayhook

        self.appName = "I18N sample application"  # pylint: disable=attribute-defined-outside-init

        self.locale = None  # pylint: disable=attribute-defined-outside-init
        wx.Locale.AddCatalogLookupPathPrefix('locale')
        lang = get_current_language_short_code()
        self.update_language(lang)

        return True

    def update_language(self, lang):
        """
        Update the language to the requested one.

        Make *sure* any existing locale is deleted before the new
        one is created.  The old C++ object needs to be deleted
        before the new one is created, and if we just assign a new
        instance to the old Python variable, the old C++ locale will
        not be destroyed soon enough, likely causing a crash.

        :param string `lang`: one of the supported language codes

        """
        # if an unsupported language is requested default to English
        if lang in constants.SUPPORTED_LANGUAGES:
            sel_lang = constants.SUPPORTED_LANGUAGES[lang]
        else:
            sel_lang = wx.LANGUAGE_ENGLISH

        if self.locale:
            assert sys.getrefcount(self.locale) <= 2
            del self.locale

        # create a locale object for this language
        self.locale = wx.Locale(sel_lang)   # pylint: disable=attribute-defined-outside-init
        if self.locale.IsOk():
            self.locale.AddCatalog(app_const.LANGUAGE_DOMAIN)
        else:
            self.locale = None  # pylint: disable=attribute-defined-outside-init

def get_current_language_short_code():
    """
    Converts the current locale name to the short language code
    suitable for our purposes
    """
    loc = locale.getlocale()
    lang = loc[0]
    short_code = lang.split('_')[0]
    return short_code
