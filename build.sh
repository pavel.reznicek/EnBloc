#!/bin/bash

VERSION=2.1.2

if [[ -z `which pandoc` ]]
then
{
	echo "PanDoc not found. Please install PanDoc first."
	exit 1
}
fi
pandoc -f markdown -o README.html README.md
pandoc -f markdown -o README.cs.html README.cs.md

if [[ -z `which pyinstaller` ]]
then
{
	echo "PyInstaller not found. Please install PyInstaller first."
	exit 1
}
fi

BINARY="en_bloc-$VERSION-`uname -m`"
LIBDIR="lib/`uname -s`-`uname -m`"
pyinstaller --name "$BINARY" --clean \
	--add-data images/*.png:images/ \
	--add-data images/*.ico:images/ \
	--add-data sounds/*.wav:sounds/ \
	--add-data locale/cs/LC_MESSAGES/*.mo:locale/cs/LC_MESSAGES/ \
	--add-data *.html:./ \
	--add-binary "$LIBDIR"/*.so*:"$LIBDIR"/ \
	--icon images/en_bloc.ico \
	--onefile app.py # && \
# ./upload.sh "dist/$BINARY"
