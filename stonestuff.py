#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Declares the Stone object for use in the background."""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"
__version__ = "0.0"

import wx
from en_bloc import constants


class Stone:  # pylint: disable=too-many-instance-attributes
    """A data representation class of a single stone"""

    def __init__(self):
        self._kind = constants.SK_SPIRAL
        self._colour = constants.SC_RED
        self._exploding = False
        self._explosion_level = 0
        self._pos = wx.Point()
        self._playground = None

    def get_kind(self):
        """The Kind property getter"""
        return self._kind

    def set_kind(self, new_kind):
        """The Kind property setter"""
        self._kind = new_kind

    Kind = property(get_kind, set_kind, doc="The stone kind (defines the shape mainly)")

    def get_colour(self):
        """The Colour property getter"""
        return self._colour

    def set_colour(self, new_colour):
        """The Colour property setter"""
        self._colour = new_colour

    Colour = property(get_colour, set_colour, doc="The stone colour")

    def get_exploding(self):
        """The Exploding property getter"""
        return self._exploding

    def set_exploding(self, new_value):
        """The Exploding property setter"""
        self._exploding = new_value

    Exploding = property(
        get_exploding, set_exploding,
        doc="A flag indicating if the stone is currently exploding"
    )

    def get_explosion_level(self):
        """The ExplosionLevel property getter"""
        return self._explosion_level

    def set_explosion_level(self, new_level):
        """The ExplosionLevel property setter"""
        if new_level < 0:
            self._explosion_level = 0
        elif new_level > 2:
            self._explosion_level = 2
        else:
            self._explosion_level = new_level

    ExplosionLevel = property(
        get_explosion_level, set_explosion_level,
        doc="The explosion level (used only if the stone is exploding)"
    )

    def get_explosion_kind(self):
        """The ExplosionKind property getter"""
        return stone_kind_to_explosion_kind(self._kind)

    ExplosionKind = property(get_explosion_kind, doc="The explosion kind (read only)")

    def get_pos(self):
        """The Pos property getter"""
        return self._pos

    def set_pos(self, new_pos):
        """The Pos property setter.
Doesnʼt actually move the stone;
use the move method to accomplish it."""
        new_pos_point = wx.Point(new_pos)
        self._pos = new_pos_point

    Pos = property(get_pos, set_pos, doc="The stone position (coordinate): wx.Point")

    def get_playground(self):
        """The Playground property getter"""
        return self._playground

    def set_playground(self, new_playground):
        """The Playground property setter"""
        self._playground = new_playground

    Playground = property(get_playground, set_playground, doc="The playground the stone is in")

    def draw(self, dst_dc):
        """Draws the stone on a destination device context"""
        self._playground.StonePaintDC.draw_stone(self, dst_dc)

    def move(self, new_pos):
        """The stone uses the playground’s move method to move itself."""
        self._playground.move_stone(self.Pos, new_pos)


def stone_kind_to_explosion_kind(stone_kind):
    """Stone kind to explosion kind converter"""
    if stone_kind < constants.MIN_STONE_KIND \
    or stone_kind > constants.MAX_STONE_KIND:
        return constants.EK_VARIANT
    return stone_kind
