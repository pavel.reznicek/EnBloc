#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"List manipulation utilities."

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"
__version__ = "0.0"

def rotate_list(the_list, count):
    """Turns the given list by given item count.
    It’s no mirroring but rotation,
    while the list is taken as a closed cycle
    vhich undergoes a turn (rotation).
"""
    if not isinstance(the_list, list):
        the_list = list(the_list)
    count = count % len(the_list)
    if count == 0:
        return the_list[:]
    if count > 0:
        return the_list[-count:] + the_list[:-count]
    return the_list[:-count] + the_list[-count:]
    
