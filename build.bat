@echo off
set VERSION=2.1.2
pandoc -f markdown -o README.html README.md
set LIBDIR=lib/Windows-%PROCESSOR_ARCHITECTURE%
pyinstaller --name en_bloc-%VERSION%-%PROCESSOR_ARCHITECTURE% --clean --add-data images/*.png;images/ --add-data images/*.ico;images/ --add-data sounds/*.wav;sounds/ --add-data *.html;./ --add-binary %LIBDIR%/*.dll;%LIBDIR%/ --icon images/en_bloc.ico --onefile app.py
pause
