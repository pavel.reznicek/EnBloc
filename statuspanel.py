#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""En bloc: A panel showing various status information, including the next block"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"

import wx
from en_bloc import constants
from en_bloc import playgroundstuff
from en_bloc import blockstuff

_ = wx.GetTranslation

class StatusPanel(wx.Panel):  # pylint: disable=too-many-instance-attributes
    """A panel showing various status information"""

    def __init__(  # pylint: disable=too-many-arguments,too-many-locals
            self, parent=None, size=(100, -1), style=wx.BORDER_RAISED,
            stone_kind=constants.SK_SPIRAL,
            stone_colours=tuple(constants.STONE_COLOURS)
    ):
        wx.Panel.__init__(self, parent=parent, size=size, style=style)
        bgnd_bmp = self._bgnd_bmp = wx.Bitmap(constants.BMP_PATH_BACKGROUND)

        szr = self.Sizer = wx.BoxSizer(wx.VERTICAL)
        spacing = 8

        st_next_block = self.st_next_block = wx.StaticText(self, label=_("Next Block:"))
        szr.Add(st_next_block, 0, wx.CENTRE | wx.TOP, spacing)

        border_size = self.Size - self.ClientSize
        pan_pgnd = self._pan_pgnd = wx.Panel(
            self,
            size=(
                constants.STONE_DIM + border_size.Width,
                constants.STONE_DIM * 4 + border_size.Height
            ),
            style=wx.BORDER_RAISED
        )
        szr.Add(pan_pgnd, 0, wx.CENTRE | wx.TOP, spacing)

        pgnd = self._pgnd = playgroundstuff.Playground(wx.Size(1, 4), bgnd_bmp)
        pgnd.StoneKind = stone_kind
        pgnd.StoneColours = stone_colours

        st_score_label = self.st_score_label = wx.StaticText(self, label=_("Score:"))
        szr.Add(st_score_label, 0, wx.CENTRE | wx.TOP, spacing)

        st_score = self.st_score = wx.StaticText(self, label="0")
        st_score_font = wx.Font(wx.FontInfo(12).Bold())
        st_score.Font = st_score_font
        st_score.ForegroundColour = wx.GREEN
        szr.Add(st_score, 0, wx.CENTRE | wx.TOP, spacing)

        self._score = 0

        self._block = None
        self.create_new_block()

        pan_pgnd.Bind(wx.EVT_PAINT, self.on_paint)

    def on_paint(self, evt):
        """The paint event handler.
Called if the new block panel gets repainted."""
        bpdc = wx.BufferedPaintDC(evt.EventObject)
        pgnd = self._pgnd
        pgnd_dc = pgnd.PlaygroundSrcDC
        pgnd_size = pgnd.Size
        pgnd_wdth = pgnd_size.Width * constants.STONE_DIM
        pgnd_hght = pgnd_size.Height * constants.STONE_DIM
        bpdc.Blit(0, 0, pgnd_wdth, pgnd_hght, pgnd_dc, 0, 0)

    def create_new_block(self):
        "Creates a new block"
        self._block = blockstuff.VerticalTetriBlock(self._pgnd)
        self._pgnd.draw()
        self._pan_pgnd.Refresh()

    def get_block(self):
        "The Block property getter"
        return self._block

    Block = property(get_block, doc="The new block waiting in queue.")

    def get_score(self):
        "The Score property getter"
        return self._score

    def set_score(self, new_score):
        "The Score property setter"
        if new_score != self._score:
            self._score = new_score
            self.st_score.LabelText = str(new_score)
            self.Sizer.Layout()

    Score = property(get_score, set_score, doc="The current score shown on the panel")

    @property
    def StoneKind(self):
        "The kind of the stones used in the new block playground"
        pgnd = self._pgnd
        return pgnd.StoneKind

    @StoneKind.setter
    def StoneKind(self, new_kind):
        "The StoneKind property setter"
        pgnd = self._pgnd
        pgnd.StoneKind = new_kind

    @property
    def StoneColours(self):
        "The colours of the stones used in the new block playground"
        pgnd = self._pgnd
        return pgnd.StoneColours

    @StoneColours.setter
    def StoneColours(self, new_colours):
        "The StoneColours property setter"
        pgnd = self._pgnd
        pgnd.StoneColours = new_colours
