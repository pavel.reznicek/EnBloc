#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The En bloc settings dialog window"""

__author__ = "Pavel Řezníček"
__copyright__ = "Copyright © 2015–2020 Pavel Řezníček"
__license__ = "GNU/GPL v. 3"


import wx
from en_bloc import settings
from en_bloc import constants

_ = wx.GetTranslation


class SettingsDialog(wx.Dialog):  # pylint: disable=too-many-ancestors
    """The En bloc settings dialog window"""

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent=parent, title=_("En bloc Settings"))
        self._szr_buttons = self.CreateSeparatedButtonSizer(flags=wx.OK | wx.CANCEL)
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(main_sizer)

        # Sizer of the main controls
        self._szr_contents = wx.BoxSizer(wx.VERTICAL)

        spacing = 8  # A constant spacing of 8 pixels between the controls

        # The stone kind static text
        self._st_stone_kind = wx.StaticText(self, label=_("&Stone Kind and Colours:"))
        main_sizer.Add(
            self._st_stone_kind, 0,
            wx.CENTRE | wx.TOP | wx.LEFT | wx.RIGHT, spacing
        )

        # The stone kind chooser
        self._stone_chooser = StoneKindChooser(self)
        self._szr_contents.Add(
            self._stone_chooser, 0,
            wx.CENTRE
        )

        # The delayed explosions checkbox
        self._cb_delayed_explosions = wx.CheckBox(self, label=_("&Delayed Explosions"))
        self._szr_contents.Add(
            self._cb_delayed_explosions, 0,
            wx.CENTRE | wx.LEFT | wx.RIGHT | wx.TOP, spacing
        )

        # The sound enabled checkbox
        self._cb_sound_enabled = wx.CheckBox(self, label=_("&Sound Enabled"))
        self._szr_contents.Add(
            self._cb_sound_enabled, 0,
            wx.CENTRE | wx.ALL, spacing
        )

        # Add the subsizers to the main sizer
        main_sizer.Add(self._szr_contents, 1, wx.GROW)
        main_sizer.Add(self._szr_buttons, 0, wx.GROW | wx.BOTTOM, 12)

        # Fit the window
        main_sizer.Fit(self)

        # The settings config (see the Settings property)
        self._settings = settings.Settings()

        # Load the settings from the config
        self.load_settings()

        self.Bind(wx.EVT_BUTTON, self.on_ok_button, id=wx.ID_OK)

    @property
    def Settings(self):
        """The Settings object"""
        return self._settings

    def on_ok_button(self, evt):
        """Fired if the OK button gets pressed."""
        response = wx.MessageBox(
            _("This will start a new game. Do you want to continue?"),
            caption=_("Confirmation"),
            style=wx.OK | wx.CANCEL | wx.OK_DEFAULT | wx.CENTRE
        )
        if response == wx.OK:
            self.save_settings()
            evt.Skip()
        # else we don't propagate the event with evt.Skip()

    def load_settings(self):
        """Loads the settings from the configuration file"""
        self._cb_delayed_explosions.SetValue(self._settings.DelayedExplosions)
        self._stone_chooser.StoneKind = self._settings.StoneKind
        for colour in constants.STONE_COLOURS:
            colour_use = self._settings.get_stone_colour_use(colour)
            self._stone_chooser.set_stone_colour_use(colour, colour_use)
        self._cb_sound_enabled.SetValue(self._settings.SoundEnabled)

    def save_settings(self):
        """Saves the settings to the configuration file"""
        self._settings.DelayedExplosions = self._cb_delayed_explosions.GetValue()
        self._settings.StoneKind = self._stone_chooser.StoneKind
        for colour in constants.STONE_COLOURS:
            colour_use = self._stone_chooser.get_stone_colour_use(colour)
            self._settings.set_stone_colour_use(colour, colour_use)
        self._settings.SoundEnabled = self._cb_sound_enabled.GetValue()
        self._settings.Flush()


class StoneKindChooser(wx.Control):
    """A stone kind and colour chooser"""

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.SetSizer(wx.GridBagSizer())

        # The stones static bitmap
        bmp_stones = wx.Bitmap(constants.BMP_PATH_TRANSPARENT_STONES)
        self._sbmp_stones = wx.StaticBitmap(self, bitmap=bmp_stones)
        self.GetSizer().Add(self._sbmp_stones, pos=(0, 1), flag=wx.GROW)

        # The radio button size (x=STONE_DIM, y=STONE_DIM)
        tb_size = wx.Size((constants.STONE_DIM,) * 2)

        # The left-side stone kind choices
        szr_kinds = self._szr_stone_kinds = wx.BoxSizer(wx.VERTICAL)
        tb_kinds = self._tb_kinds = \
            [wx.ToggleButton(self, id=kind, size=tb_size) for kind in constants.STONE_KINDS]
        # tb_kinds[constants.SK_DIAGONAL].Value = True
        szr_kinds.AddMany(tb_kinds)
        self.GetSizer().Add(szr_kinds, pos=(0, 0), flag=wx.GROW)

        STONE_BRUSHES = [
            wx.RED_BRUSH,
            wx.GREEN_BRUSH,
            wx.BLUE_BRUSH,
            wx.GREY_BRUSH,
            wx.Brush(wx.Colour(255, 255, 0)), # yellow
            wx.WHITE_BRUSH
        ]
        
        # The bottom-side stone colour choices
        szr_colours = self._szr_colours = wx.BoxSizer(wx.HORIZONTAL)
        tb_colours = self._tb_colours = \
            [wx.ToggleButton(self, id=colour, size=tb_size) for colour in constants.STONE_COLOURS]
        for tb_colour in tb_colours:
            tb_colour.SetValue(True)
            bmp = wx.Bitmap(tb_size)
            mdc = wx.MemoryDC(bmp)
            mdc.Clear()
            mdc.SetBrush(STONE_BRUSHES[tb_colour.GetId()])
            mdc.FloodFill(0, 0, wx.WHITE)
            tb_colour.SetBitmap(bmp)
        szr_colours.AddMany(tb_colours)
        self.GetSizer().Add(szr_colours, pos=(1, 1), flag=wx.GROW)

        # Bind event handlers
        for tb_kind in tb_kinds:
            tb_kind.Bind(wx.EVT_TOGGLEBUTTON, self.on_kind_toggle)
        for tb_colour in tb_colours:
            tb_colour.Bind(wx.EVT_TOGGLEBUTTON, self.on_colour_toggle)

    def on_kind_toggle(self, evt):
        """The stone kind button toggle handler"""
        button = evt.EventObject
        tb_kinds = self._tb_kinds
        if button.Value:  # select
            deselect_all_tb_except(tb_kinds, [button])
        else:  # deselect
            count = get_sel_tbs_count(tb_kinds)
            if count < 1:
                button.Value = True

    def on_colour_toggle(self, evt):
        """The stone colour button toggle handler"""
        button = evt.EventObject
        tb_colours = self._tb_colours
        if not button.Value:  # deselect
            count = get_sel_tbs_count(tb_colours)
            if count < 2:
                button.Value = True

    @property
    def StoneKind(self):
        """The StoneKind property getter"""
        selected = get_selected_tbs(self._tb_kinds)[0]
        return selected.Id

    @StoneKind.setter
    def StoneKind(self, new_kind):
        """The StoneKind property setter"""
        self._tb_kinds[new_kind].Value = True

    @property
    def StoneColours(self):
        """The StoneColours property getter"""
        selected = get_selected_tbs(self._tb_colours)
        result = {tbtn.Id for tbtn in selected}
        return result

    @StoneColours.setter
    def StoneColours(self, new_colours):
        """Thee StoneColours property setter"""
        tb_colours = self._tb_colours
        for colour in constants.STONE_COLOURS:
            tb_colours[colour].Value = colour in new_colours

    def get_stone_colour_use(self, colour):
        """Get the stone colour use flag"""
        result = self._tb_colours[colour].Value
        return result

    def set_stone_colour_use(self, colour, new_use):
        """Set the stone colour use flag"""
        self._tb_colours[colour].Value = new_use


def deselect_all_tb_except(tb_group, excepted):
    """Deselect all toggle buttons in tb_group, ignoring excepted"""
    for tbtn in tb_group:
        if tbtn not in excepted:
            tbtn.Value = False


def get_selected_tbs(tb_group):
    """Get the selected toggle buttons in tb_group"""
    result = [tbtn for tbtn in tb_group if tbtn.Value]
    return result


def get_sel_tbs_count(tb_group):
    """Get the selected toggle buttons count in tb_group"""
    selected = get_selected_tbs(tb_group)
    return len(selected)
