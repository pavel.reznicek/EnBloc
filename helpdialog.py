#!/usr/bin/env python3
#-*- coding: utf-8 -*-

"The help dialog for the En bloc game"

import wx
import wx.html
from en_bloc import constants

_ = wx.GetTranslation

class HelpDialog(wx.Dialog): # pylint: disable=too-few-public-methods,too-many-ancestors
    "Shows the README help and the LICENSE in the HTML format."
    def __init__(self, parent):
        super().__init__(
            parent=parent, style=wx.RESIZE_BORDER,
            size=(640, 480), title=_("En bloc Help")
        )

        self.Sizer = wx.BoxSizer(wx.VERTICAL)

        ntb = self._ntb = wx.Notebook(self)
        self.Sizer.Add(ntb, 1, wx.GROW)

        pg_readme = self._pg_readme = wx.html.HtmlWindow(ntb)
        readme_file = open(constants.get_international_html_path_readme(), encoding="utf-8")
        readme_text = readme_file.read()
        pg_readme.SetPage(readme_text)
        ntb.AddPage(pg_readme, _("Read Me"))
        pg_license = self._pg_license = wx.html.HtmlWindow(ntb)
        license_file = open(constants.HTML_PATH_LICENSE, encoding="utf-8")
        license_text = license_file.read()
        pg_license.SetPage(license_text)
        ntb.AddPage(pg_license, _("License"))

        szr_buttons = self._szr_buttons = self.CreateSeparatedButtonSizer(wx.CLOSE)
        self.Sizer.Add(szr_buttons, 0, wx.GROW)

if __name__ == '__main__':
    APP = wx.App()
    DLG = HelpDialog(None)
    DLG.ShowModal()
